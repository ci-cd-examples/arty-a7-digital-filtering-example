# Arty A7 Example Design
## Hardware Requirements
1. [Arty A7 evaluation board](https://store.digilentinc.com/arty-a7-artix-7-fpga-development-board-for-makers-and-hobbyists/)
2. [Pmod AD1](https://store.digilentinc.com/pmod-ad1-two-12-bit-a-d-inputs/)
3. [Pmod DA4](https://store.digilentinc.com/pmod-da4-eight-12-bit-d-a-outputs/)
4. [Analog Discovery 2](https://store.digilentinc.com/analog-discovery-2-100msps-usb-oscilloscope-logic-analyzer-and-variable-power-supply/) (or another function generator)

## Hardware Setup
1. Connect the Pmod AD1 to the JA connector (pins 7-12)
2. Connect the Pmod DA4 to the JD connector (pins 7-12)
3. Connect the Pmod AD1 pins to the Analog Discovery 2 like so:

| Pmod AD1 | Analog Discovery 2 |
| -------- | ------------------ |
| A0       | W1                 |
| GND      | GND                |

4. Connect the Pmod DA4 pins to the Analog Discovery 2 like so:

| Pmod DA4 | Analog Discovery 2 |
| -------- | ------------------ |
| A        | 1+                 |
| B        | 2+                 |
| GND      | 1-                 |
| GND      | 2-                 |

## How to build locally using Docker
```
docker run --volume /tools/Xilinx:/mnt/efs/vivado_2019_1 --volume /tools:/tools --volume `pwd`:/arty_example --workdir /arty_example -it registry.gitlab.com/fpga-tools/docker-vivado-2019-1 wget https://gitlab.com/fpga-tools/Xilinx-Build-System/-/raw/master/build.tcl && mkdir vivado && cd vivado && vivado -mode batch -source ../build.tcl -notrace -tclargs arty_a7 build
```

## How to test
1. Drive a Sine wave (varying from 1-5 KHz) from the Analog Discovery 2 into the AD1 Pmod. Set amplitude and offset to 1.5 V
2. Observe Channel 1 (DA4 Channel A) should be a slightly attenuated version of the original Sine wave
Note: This is just the AD1 digitizing the input signal and then passing it directly to the DA Pmod. Dynamic range is attenuate due to max voltage output on the Pmod DA4 being lower than the input voltage of the AD1
3. Observe Channel 2 as the filtered output of the inputted sine wave.