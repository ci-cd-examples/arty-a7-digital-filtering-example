// ------------------------------------------------------------------
// axis_pmod_da4.sv
//
// 8/7/2018 D. W. Hawkins (dwh@caltech.edu)
//
// Modified By: Ari Mahpour
// Modified Data: 11/30/2020
//
// Digilent PmodDA4 AXI4-Stream interface.
//
// Data words received over the AXI4-Stream interface are
// serialized over the SPI bus to the AD5628 DAC. The format
// of the 32-bit word is defined in the DAC datasheet.
// The 32-bit word provides DAC function control, eg., enabling
// the internal reference, as well as DAC output voltage control.
//
// ------------------------------------------------------------------
// References
// ----------
//
// [1] Digilent, "Pmod DA4: Eight 12-bit D/A Outputs",
//     https://store.digilentinc.com/pmod-da4-eight-12-bit-d-a-outputs
//
// [2] Digilent, "Pmod DA4 Reference Manual",
//     https://reference.digilentinc.com/reference/pmod/pmodda4/reference-manual
//     https://reference.digilentinc.com/_media/reference/pmod/pmodda4/pmodda4_rm.pdf
//
// [3] Digilent, "Pmod DA4 Schematic",
//     https://reference.digilentinc.com/_media/reference/pmod/pmodda4/pmodda4_sch.pdf
//
// [4] Analog Devices, "Octal, 12-/14-/16-Bit, SPI Voltage
//     Output denseDAC with 5 ppm/�C, On-Chip Reference",
//     Rev J, 2017.
//     http://www.analog.com/AD5628
//
// ------------------------------------------------------------------

`include "interfaces.sv"

module axis_pmod_da4 (
      spi_stream_if.spi_writer ports
   );

   // ---------------------------------------------------------------
   // Internal signals
   // ---------------------------------------------------------------
   //
   // Bit counter
   logic  [4:0] bit_d;
   logic  [4:0] bit_q;
   //
   // Shift register and bit counter
   logic [31:0] shift_q;
   logic        shift_load;
   logic        shift_en;
   logic        shift_done;

   // ---------------------------------------------------------------
   // Control FSM
   // ---------------------------------------------------------------
   //
   axis_pmod_da4_fsm u1 (
      .rst_n      (ports.rst_n     ),
      .clk        (ports.clk       ),
      .axis_valid (ports.axis_valid),
      .axis_ready (ports.axis_ready),
      .spi_sel_n  (ports.spi_sel_n ),
      .spi_sck    (ports.spi_sck   ),
      .shift_load (shift_load),
      .shift_en   (shift_en  ),
      .shift_done (shift_done)
   );

   // ---------------------------------------------------------------
   // Bit counter
   // ---------------------------------------------------------------
   //
   axi4_counter #(
      .WIDTH(5)
   ) u2 (
      .rst_n (ports.rst_n     ),
      .clk   (ports.clk       ),
      .load  (shift_load),
      .en    (shift_en  ),
      .dir   (1'b0      ), // Down counter
      .cin   (1'b1      ),
      .d     (bit_d     ),
      .q     (bit_q     ),
      .cout  (shift_done)
   );

   // Load value
   assign bit_d = 5'd31;

   // ---------------------------------------------------------------
   // Shift register
   // ---------------------------------------------------------------
   //
   always_ff @(negedge ports.rst_n or posedge ports.clk) begin
      if (~ports.rst_n) begin
         shift_q <= '0;
      end
      else begin
         if (shift_load) begin
//          shift_q <= {4'h0, ports.axis_data[27:0]};
            shift_q <= ports.axis_data;
         end
         else if (shift_en) begin
            shift_q <= {shift_q[30:0], 1'b0};
         end
      end
   end

   // SPI output data
   assign ports.spi_mosi = shift_q[31];

endmodule
