// ------------------------------------------------------------------
// axis_pmod_da4_fsm.sv
//
// 8/7/2018 D. W. Hawkins (dwh@caltech.edu)
//
// Digilent PmodDA4 AXI4-Stream interface.
//
// ------------------------------------------------------------------

module axis_pmod_da4_fsm (
      // ------------------------------------------------------------
      // AXI4-Stream Reset and Clock
      // ------------------------------------------------------------
      //
      input  logic rst_n,
      input  logic clk,

      // ------------------------------------------------------------
      // AXI4-Stream Slave
      // ------------------------------------------------------------
      //
      input  logic axis_valid,
      output logic axis_ready,

      // ------------------------------------------------------------
      // SPI Write-only Interface
      // ------------------------------------------------------------
      //
      output logic spi_sel_n,
      output logic spi_sck,

      // ------------------------------------------------------------
      // Datapath Control
      // ------------------------------------------------------------
      //
      output logic shift_en,
      output logic shift_load,
      input  logic shift_done

   );

   // ------------------------------------------------------------
   // FSM enumeration
   // ------------------------------------------------------------
   //
   enum {
      S_IDLE,
      S_START,  // Enforce SYNC-to-SCK delay
      S_HIGH,   // SCK is high
      S_LOW,    // SCK is low
      S_END     // SEL deasserted
   } state = S_IDLE;

   // ------------------------------------------------------------
   // Internal signals
   // ------------------------------------------------------------
   //
   // Combinatorial signals
   logic c_axis_ready = 1'b1;
   logic c_spi_sel_n  = 1'b1;
   logic c_spi_sck    = 1'b1;
   logic c_shift_en   = 1'b0;
   logic c_shift_load = 1'b0;

   // ------------------------------------------------------------
   // FSM state transitions
   // ------------------------------------------------------------
   //
   always_ff @(negedge rst_n or posedge clk)
   begin
      if (~rst_n) begin
         state <= S_IDLE;
      end
      else begin
         case (state)

            // Wait for valid data
            S_IDLE:
               if (axis_valid) begin
                  state <= S_START;
               end

            // Select to SCK delay
            S_START:
               begin
                  state <= S_HIGH;
               end

            // SCK is high
            S_HIGH:
               begin
                  state <= S_LOW;
               end

            // SCK is low
            S_LOW:
               begin
                  if (~shift_done) begin
                     state <= S_HIGH;
                  end
                  else begin
                     state <= S_END;
                  end
               end

            // Deassert SPI controls
            S_END:
               begin
                  state <= S_IDLE;
               end
         endcase
      end
   end

   // ------------------------------------------------------------
   // FSM combinatorial output generation
   // ------------------------------------------------------------
   //
   always_comb
   begin
      // Defaults
      c_axis_ready = 1'b1; // Ready for AXI4-Stream data
      c_spi_sel_n  = 1'b1;
      c_spi_sck    = 1'b1;
      c_shift_en   = 1'b0;
      c_shift_load = 1'b0;

      // Output generation
      case (state)
         // Wait for valid data
         S_IDLE:
            if (axis_valid) begin
               // Not ready
               c_axis_ready = 1'b0;

               // Assert SPI select/sync
               c_spi_sel_n = 1'b0;

               // Load the shift-register and bit-counter
               c_shift_load = 1'b1;
            end

         // Select to SCK delay
         S_START:
            begin
               // Not ready
               c_axis_ready = 1'b0;

               // Assert SPI select/sync
               c_spi_sel_n = 1'b0;
            end

         // SCK is high
         S_HIGH:
            begin
               // Not ready
               c_axis_ready = 1'b0;

               // Assert SPI select/sync
               c_spi_sel_n = 1'b0;

               // SPI clock low
               c_spi_sck = 1'b0;
            end

         // SCK is low
         S_LOW:
            begin
               // Not ready
               c_axis_ready = 1'b0;

               if (~shift_done) begin
                  // Assert SPI select/sync
                  c_spi_sel_n = 1'b0;

                  // Enable the shift-register and bit-counter
                  c_shift_en = 1'b1;
               end
            end

         // Deassert SPI controls
         S_END:
            begin
               ; // Wait
            end

      endcase
   end

   // ------------------------------------------------------------
   // Outputs
   // ------------------------------------------------------------
   //
   // Registered outputs
   always_ff @(negedge rst_n or posedge clk)
   begin
      if (~rst_n) begin
         axis_ready = 1'b0; // Not ready for AXI4-Stream data
         spi_sel_n  = 1'b1;
         spi_sck    = 1'b1;
      end
      else begin
         axis_ready <= c_axis_ready;
         spi_sel_n  <= c_spi_sel_n;
         spi_sck    <= c_spi_sck;
      end
   end

   // Combinatorial outputs
   assign shift_en   = c_shift_en;
   assign shift_load = c_shift_load;

endmodule