// Module to drive Pmod DA4 traffic (including initialization)
// The state machine runs in a round robin to send all NUM_CHAN channels
// of DAC data to the Pmod DA4 device (AD5628 IC)

`include "interfaces.sv"

module pmod_da4_driver #(
    parameter NUM_CHAN = 8
)
(
    input  logic clk,
    input  logic rst_n,
    input  logic [11:0] data [0:NUM_CHAN-1],

    // SPI Write-only Interface
    output logic spi_sel_n,
    output logic spi_sck,
    output logic spi_mosi
);
    // AD5628 Command Definitions
    // 0 0 0 0 Write to Input Register n
    // 0 0 0 1 Update DAC Register n
    // 0 0 1 0 Write to Input Register n, update all (software LDAC)
    // 0 0 1 1 Write to and update DAC Channel n
    // 0 1 0 0 Power down/power up DAC
    // 0 1 0 1 Load clear code register
    // 0 1 1 0 Load LDAC register
    // 0 1 1 1 Reset (power-on reset)
    // 1 0 0 0 Set up internal REF register
    // 1 0 0 1 Reserved
    // – – – – Reserved
    // 1 1 1 1 Reserved
    localparam AD5628_CMD_WRITE               = 4'b0000;
    localparam AD5628_CMD_UPDATE              = 4'b0001;
    localparam AD5628_CMD_WRITE_UPDATE_ALL    = 4'b0010;
    localparam AD5628_CMD_WRITE_UPDATE        = 4'b0011;
    localparam AD5628_CMD_POWER               = 4'b0100;
    localparam AD5628_CMD_LOAD_CC             = 4'b0101;
    localparam AD5628_CMD_LOAD_LDAC           = 4'b0110;
    localparam AD5628_CMD_RESET               = 4'b0111;
    localparam AD5628_CMD_SET_REF             = 4'b1000;
    localparam AD5628_INIT_CMD                = 32'h0800_0001;  // AXI Inititlization command (for DA4 only)

    logic [3:0] channel_counter;

    // States for sending AXI-Stream data to Pmod DA4
    enum logic [1:0] {
        INIT = 2'b00,
        WAIT = 2'b01,
        SEND_DATA = 2'b10
    } STATE;


    spi_stream_if da4_if(.rst_n(rst_n), .clk(clk));

    // Hook up all passthroughs to interface
    assign spi_sel_n = da4_if.spi_sel_n;
    assign spi_sck = da4_if.spi_sck;
    assign spi_mosi = da4_if.spi_mosi;

    // Instantiate AXI-Stream SPI core for DA4 Pmod
    axis_pmod_da4 da4 (da4_if);

    always_ff @ (posedge clk or negedge rst_n) begin
        if (!rst_n) begin
            channel_counter <= 0;
            STATE <= INIT;
            da4_if.axis_valid <= 0;
            da4_if.axis_last <= 0;
            da4_if.axis_data <= 0;
        end
        else begin
            // Initialize the Pmod DA4 with the internal refrence
            if (STATE == INIT) begin
                da4_if.axis_valid <= 1;
                da4_if.axis_data <= AD5628_INIT_CMD;
                STATE <= WAIT;
            end
            // Wait an extra clock to de-assert the axis_valid signal
            else if (STATE == WAIT) begin
                da4_if.axis_valid <= 0;
                STATE <= SEND_DATA;
            end
            // Send the DAC out data
            else begin
                // Default values for waiting state
                da4_if.axis_valid <= 0;
                STATE <= STATE;

                // Ensure AXI-Stream interface is ready to receive data
                if (da4_if.axis_ready) begin
                    // Round robin through all data channels
                    da4_if.axis_valid <= 1;
                    da4_if.axis_data <= {4'b0, AD5628_CMD_WRITE_UPDATE, channel_counter, data[channel_counter], 8'b0};

                    // Reset or increment the counter
                    if (channel_counter >= NUM_CHAN-1) 
                        channel_counter <= 0;
                    else
                        channel_counter <= channel_counter + 1;

                    // Wait one clock cycle for response signals to register
                    STATE <= WAIT;
                end
            end
        end
    end

endmodule