// Arty A7 Pmod AD1/DA4 Filter example

`timescale 1ns / 1ps

`include "interfaces.sv"

module top (
    // Onboard oscillator
    input logic  CLK100MHZ,

    // ChipKit Single Ended Analog Inputs
    input logic  [8:0] ck_an_p,
    input logic  [8:0] ck_an_n,

    // Dedicated ADC channel
    input logic  vp_in,
    input logic  vn_in,

    // Onboard switches and LEDs
    input logic  [3:0] sw,
    input logic  [3:0] btn,
    output logic [7:0] LED,

    // Pmod port signals
    output logic ja_7,
    input  logic ja_8,
    input  logic ja_9,
    output logic ja_10,
    output logic jd_7,
    output logic jd_8,
    output logic jd_9,
    output logic jd_10
 );

    localparam ADC_RES_BITS = 16;                                           // Bit width resolution of targetted ADC (as instructed by FIR compiler)
    localparam ADC_RES_BITS_REAL = 12;                                      // Actual bit width resolution of ADC
    localparam SAT_HI = (2**ADC_RES_BITS_REAL)-1;                           // High value to saturate (i.e. clip) to
    localparam SAT_LOW = 0;                                                 // Low value to saturate (i.e. clip) to

    // ---------------------
    // FIR Filter parameters
    // ---------------------
    localparam COEFF_BIT_WIDTH = 18;                                        // Bit width of FIR filter coefficients
    // Bit width for FIR filter input data (incremented by set of 8 bits for AXI)
    localparam INPUT_BIT_WIDTH = ADC_RES_BITS % 8 > 0 ? int'($ceil(ADC_RES_BITS/8.0)*8) : ADC_RES_BITS;
    // Bit width for FIR filter output data (incremented by set of 8 bits for AXI)
    localparam OUTPUT_BIT_WIDTH = (ADC_RES_BITS + COEFF_BIT_WIDTH) % 8 > 0 ? int'($ceil((ADC_RES_BITS + COEFF_BIT_WIDTH)/8.0)*8) : ADC_RES_BITS + COEFF_BIT_WIDTH;
    localparam MSB_FILTER = INPUT_BIT_WIDTH + (ADC_RES_BITS-1);             // MSB of filtered data to start pulling from
    localparam sampling_clock_hz = 10_000;                                  // ADC reading happens at a 10 KHz frequency
    localparam sampling_clock_time = 100_000_000 / sampling_clock_hz;       // Simulates 10 KHz ADC data "valid" counter

    // Top level signals and interfaces
    logic clk_ila;
    logic rst_n;
    spi_stream_if ad1_if(.rst_n(rst_n), .clk(CLK100MHZ));

    // Filtered data signals
    logic [INPUT_BIT_WIDTH-1:0] s_axis_data_tdata;
    logic [OUTPUT_BIT_WIDTH-1:0] filtered_data;
    logic [ADC_RES_BITS-1:0] filter_trunc_msb;
    int data_valid_counter;
    logic s_axis_data_tvalid;
    assign s_axis_data_tdata = ({1'b0, ad1_if.axis_data[ADC_RES_BITS-1:0]});

    // --------------------
    // Module instantiation
    // --------------------
    // Use external PMOD to read ADC channels
    axis_pmod_ad1 ad1 (ad1_if);

    // Use external PMOD to drive out waveforms and view with oscilloscope
    pmod_da4_driver #(.NUM_CHAN(2)) da4 (
        .clk        (CLK100MHZ),
        .rst_n      (rst_n),
        .data       ('{ad1_if.axis_data[11:0], filter_trunc_msb}),
        .spi_sel_n  (jd_7),
        .spi_sck    (jd_10),
        .spi_mosi   (jd_8)
    );

    // Integrated Logic Analyzer (ILA)
    ila_0 ila (
        .clk(CLK100MHZ),
        .probe0({ad1_if.axis_data[15:0], jd_10, jd_9, jd_8, jd_7, 16'b0}),
        .probe1(ad1_if.axis_valid)
    );

    fir_compiler_0 adc_filter (
        .aclk               (CLK100MHZ),                // AXI Clock
        .aresetn            (rst_n),                    // Reset (Active low)
        .s_axis_data_tvalid (s_axis_data_tvalid),       // Input: Valid data (to be raised when sample is valid)
        //.s_axis_data_tvalid (ad1_if.axis_valid),      // Input: Valid data (to be raised when sample is valid)
        .s_axis_data_tready (),                         // Output: Filter is ready for another input
        .s_axis_data_tdata  (s_axis_data_tdata),        // Input: Data to be filtered
        .m_axis_data_tvalid (),                         // Output: Data valid signal
        .m_axis_data_tdata  (filtered_data)             // Output: Filtered data (output)
    );

    // Pin assignments to onboard IOs
    assign ja_7 = ad1_if.spi_sel_n;
    assign ad1_if.spi_miso[0] = ja_8;
    assign ad1_if.spi_miso[1] = ja_9;
    assign ja_10 = ad1_if.spi_sck;
    assign jd_9 = 1'b0;
    assign rst_n = !btn[0];

    // Always be ready for science...
    // Tell AXI-Stream interface to continuously grab ADC data from Pmod AD1
    assign ad1_if.axis_ready = 1'b1;

    // Truncation handling
    always_comb begin
        // Look for the next bit above the "true" MSB to see if it goes high. That and the filter's MSB (i.e. 40th bit - which is the sign bit) going low means we're now saturated.
        if (filtered_data[INPUT_BIT_WIDTH + ADC_RES_BITS_REAL + 1] && !filtered_data[$size(filtered_data)-1])
            filter_trunc_msb = SAT_HI;
        // If the filter MSB (i.e. 40th bit - which is the sign bit) goes low that means we're now saturated at the low side.
        else if (filtered_data[$size(filtered_data)-1])
            filter_trunc_msb = SAT_LOW;
        else
            filter_trunc_msb = {filtered_data[$size(filtered_data)-1], filtered_data[MSB_FILTER -: (ADC_RES_BITS-1)]};
    end

    // 100 KHz data valid signal
    // WARNING: This is a total hack to demonstrate the FIR filter input requirements of 100 KHz
    //          For a truly implemented design the data from the SPI bus should actually be
    //          be delivered at a rate of 100 KHz (and not pinged for it at that rate).
    always @(posedge CLK100MHZ or negedge rst_n) begin
        if (!rst_n) begin
            data_valid_counter <=0;
            s_axis_data_tvalid <= 0;
        end
        else begin
            // Generate FIR compiler data_valid pulse
            if (data_valid_counter < sampling_clock_time) begin
                data_valid_counter++;
                s_axis_data_tvalid = 0;
            end
            else begin
                data_valid_counter = 0;
                s_axis_data_tvalid = 1;
            end
        end
    end
endmodule