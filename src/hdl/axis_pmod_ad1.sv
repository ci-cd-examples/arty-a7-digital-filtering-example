// ------------------------------------------------------------------
// axis_pmod_ad1.sv
//
// 8/7/2018 D. W. Hawkins (dwh@caltech.edu)
//
// Modified By: Ari Mahpour
// Modified Data: 11/30/2020
//
// Digilent PmodAD1 AXI4-Stream interface.
//
// The PmodAD1 contains two 12-bit 1MSps SPI ADCs.
// Assertion of ready on the AXI4-Stream interface initiates an
// SPI transaction that causes both ADCs to perform a conversion.
// Continuous assertion of the AXI4-Stream ready signal will cause
// back-to-back conversions at ~1MHz sample rate.
//
// ------------------------------------------------------------------
// Notes
// -----
//
// 1. AD7476A SPI timing
//
//    The ADC outputs the first data bit on the falling-edge of
//    SEL_N and all subsequent bits on the falling-edge of SCK.
//    The SEL_N-to-MISO delay for the first bit is 22ns, while
//    the SCK-to-MISO delay for all other data bits is 40ns.
//    The SCK maximum frequency is 20MHz (50ns), so the SCK-to-MISO
//    delay is nearly a whole period. The control FSM shifts
//    serial data in at the falling-edge of SCK. The control FSM
//    does not generate a shift-enable pulse on the first bit,
//    since that bit is discarded. The 12-bit shift registers are
//    enabled 15 times during the 16 SCK period SPI transaction.
//    At the end of a transaction, the shift registers are loaded
//    into a 32-bit AXI4-Stream data register.
//
// 2. AXI4-Stream ready-to-valid timing
//
//    The ADC control FSM starts a conversion when ready is asserted.
//    When the ADC conversion completes, the valid signal is
//    asserted, completing the AXI4-Stream transaction. The ADC
//    control FSM ignores the state of the valid signal until the
//    ADC is ready to perform another conversion. If the control FSM
//    returns to the idle state and ready is asserted, a new
//    conversion will be started.
//
//    To perform synchronous sampling at ~1MHz, an AXI4-DMA controller
//    can be programmed to read multiple samples from the ADC
//    interface. The ADC transactions require 17 x 6 + 1 = 103 clock
//    periods, i.e., 1.03us for a 100MHz FPGA clock. The ADC sample
//    rate is ~1MHz.
//
// ------------------------------------------------------------------
// References
// ----------
//
// [1] Digilent, "Pmod AD1: Two 12-bit A/D Inputs",
//     https://store.digilentinc.com/pmod-ad1-two-12-bit-a-d-inputs/
//
// [2] Digilent, "Pmod AD1 Reference Manual",
//     https://reference.digilentinc.com/reference/pmod/pmodad1/reference-manual
//     https://reference.digilentinc.com/_media/reference/pmod/pmodad1/pmodad1_rm.pdf
//
// [3] Digilent, "Pmod AD1 Schematic",
//     https://reference.digilentinc.com/_media/reference/pmod/pmodad1/pmodad1_sch.pdf
//
// [4] Analog Devices, "2.35 V to 5.25 V, 1 MSPS,
//     12-/10-/8-Bit ADCs in 6-Lead SC70", Rev F, 2011.
//     http://www.analog.com/AD7476A
//     http://www.analog.com/static/imported-files/data_sheets/AD7476A_7477A_7478A.pdf
//
// ------------------------------------------------------------------

`include "interfaces.sv"

module axis_pmod_ad1 (
      spi_stream_if.spi_reader ports
   );

   // ---------------------------------------------------------------
   // Internal signals
   // ---------------------------------------------------------------
   //
   // Baud counter
   logic  [1:0] baud_d;
   logic  [1:0] baud_q;
   logic        baud_load;
   logic        baud_en;
   logic        baud_done;
   //
   // Bit counter
   logic  [3:0] bit_d;
   logic  [3:0] bit_q;
   //
   // Shift registers and bit counter
   logic [11:0] shift_a;
   logic [11:0] shift_b;
   logic        shift_load;
   logic        shift_en;
   logic        shift_done;
   //
   // AXI data
   logic [31:0] data_q;
   logic        data_load;
   logic        data_valid;

   // ---------------------------------------------------------------
   // Control FSM
   // ---------------------------------------------------------------
   //
   axis_pmod_ad1_fsm u1 (
      .rst_n      (ports.rst_n      ),
      .clk        (ports.clk       ),
      .axis_ready (ports.axis_ready ),
      .spi_sel_n  (ports.spi_sel_n  ),
      .spi_sck    (ports.spi_sck    ),
      .baud_load  (baud_load        ),
      .baud_en    (baud_en          ),
      .baud_done  (baud_done        ),
      .shift_load (shift_load       ),
      .shift_en   (shift_en         ),
      .shift_done (shift_done       ),
      .data_load  (data_load        )
   );

   // ---------------------------------------------------------------
   // Baud counter
   // ---------------------------------------------------------------
   //
   axi4_counter #(
      .WIDTH(2)
   ) u2 (
      .rst_n (ports.rst_n  ),
      .clk   (ports.clk    ),
      .load  (baud_load    ),
      .en    (baud_en      ),
      .dir   (1'b0         ), // Down counter
      .cin   (1'b1         ),
      .d     (baud_d       ),
      .q     (baud_q       ),
      .cout  (baud_done    )
   );

   // Load value
   assign baud_d = 2'd2;

   // ---------------------------------------------------------------
   // Bit counter
   // ---------------------------------------------------------------
   //
   axi4_counter #(
      .WIDTH(4)
   ) u3 (
      .rst_n (ports.rst_n  ),
      .clk   (ports.clk    ),
      .load  (shift_load   ),
      .en    (shift_en     ),
      .dir   (1'b0         ), // Down counter
      .cin   (1'b1         ),
      .d     (bit_d        ),
      .q     (bit_q        ),
      .cout  (shift_done)
   );

   // Load value
   assign bit_d = 5'd15;

   // ---------------------------------------------------------------
   // Shift registers
   // ---------------------------------------------------------------
   //
   always_ff @(negedge ports.rst_n or posedge ports.clk) begin
      if (~ports.rst_n) begin
         shift_a <= '0;
         shift_b <= '0;
      end
      else begin
         if (shift_load) begin
            shift_a <= 'h0;
            shift_b <= 'h0;
         end
         else if (shift_en) begin
            // 12-bit SPI data shifted MSB-to-LSB
            shift_a <= {shift_a[10:0], ports.spi_miso[0]};
            shift_b <= {shift_b[10:0], ports.spi_miso[1]};
         end
      end
   end

   // ---------------------------------------------------------------
   // AXI4-Stream data
   // ---------------------------------------------------------------
   //
   always_ff @(negedge ports.rst_n or posedge ports.clk) begin
      if (~ports.rst_n) begin
         data_valid <= 1'b0;
         data_q     <= '0;
      end
      else begin
         if (data_load) begin
            data_valid <= 1'b1;
            // Channel A in LSBs, B in MSBs
            data_q     <= {4'h0, shift_b, 4'h0, shift_a};
         end
         if (data_valid) begin
            if (ports.axis_ready) begin
               data_valid <= 1'b0;
//             data_q     <= '0;
            end
         end
      end
   end
   assign ports.axis_valid = data_valid;
// assign ports.axis_last  = 1'b1; // AXI DMA transfers stop after 1 word
   assign ports.axis_last  = 1'b0;

   // WARNING: DEBUG ONLY! NEEDS TO BE REMOVED
   // Hacker override to reach down and generate stimulus from testbench
   `ifndef MODEL_TECH
      assign ports.axis_data  = data_q;
   `endif
endmodule
