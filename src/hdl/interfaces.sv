// Interfaces needed for the Arty A7 Filter example design

// SPI AXI4 streamer interface for Pmod SV modules
interface spi_stream_if
# (
	parameter integer C_S_AXI_DATA_WIDTH	= 32   // Width of S_AXI data bus
)
(
    input logic rst_n,      // Global Reset Signal. This Signal is Active LOW
    input logic clk         // Global Clock Signal
);
    
    // ------------------------------------------------------------
    // AXI4-Stream Slave
    // ------------------------------------------------------------
    //
    logic        axis_ready;
    logic        axis_valid;
    logic        axis_last;
    logic [C_S_AXI_DATA_WIDTH-1:0] axis_data;

    // ------------------------------------------------------------
    // SPI Read-only Interface
    // ------------------------------------------------------------
    //
    logic        spi_sel_n;
    logic        spi_sck;
    logic  [1:0] spi_miso;
    logic        spi_mosi;

    // Reads data a SPI module
    // Example: Pmod AD1
    modport spi_reader (
        // AXI4-Stream Reset and Clock
        input rst_n,
        input clk,

        // AXI4-Stream Slave
        input  axis_ready,
        output axis_valid,
        output axis_last,
        output axis_data,

        // SPI Read-only Interface
        output spi_sel_n,
        output spi_sck,
        input  spi_miso
    );

    // Writes data to a SPI module
    // Example: Pmod DA4
    modport spi_writer (
        // AXI4-Stream Reset and Clock
        input rst_n,
        input clk,

        // AXI4-Stream Slave
        output axis_ready,
        input  axis_valid,
        input  axis_last,
        input  axis_data,

        // SPI Write-only Interface
        output spi_sel_n,
        output spi_sck,
        output spi_mosi
    );

endinterface //spi_stream_if

interface ad56x8_reg_if;
    logic [3:0]  addr;
    logic [11:0] data;
    
    // Command Definitions
    // 0 0 0 0 Write to Input Register n
    // 0 0 0 1 Update DAC Register n
    // 0 0 1 0 Write to Input Register n, update all (software LDAC)
    // 0 0 1 1 Write to and update DAC Channel n
    // 0 1 0 0 Power down/power up DAC
    // 0 1 0 1 Load clear code register
    // 0 1 1 0 Load LDAC register
    // 0 1 1 1 Reset (power-on reset)
    // 1 0 0 0 Set up internal REF register
    // 1 0 0 1 Reserved
    // – – – – Reserved
    // 1 1 1 1 Reserved
    enum logic [3:0] {
        WRITE               = 4'b0000,
        UPDATE              = 4'b0001,
        WRITE_UPDATE_ALL    = 4'b0010,
        WRITE_UPDATE        = 4'b0011,
        POWER               = 4'b0100,
        LOAD_CC             = 4'b0101,
        LOAD_LDAC           = 4'b0110,
        RESET               = 4'b0111,
        SET_REF             = 4'b1000
    } cmd;

    // Initialize Pmod DA4 with internal reference with init command
    const int INIT_CMD = 32'h0800_0001;
    logic init_done;

    // Pack to 32 bits
    function logic [31:0] pack();
        return {
            4'b0,   // Don't care
            cmd,    // Command bits
            addr,   // Address bits
            data,   // Data bits
            8'b0    // Don't care
        };
    endfunction

endinterface //ad56x8_reg_if