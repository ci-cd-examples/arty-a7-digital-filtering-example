// ------------------------------------------------------------------
// axis_pmod_ad1_fsm.sv
//
// 8/7/2018 D. W. Hawkins (dwh@caltech.edu)
//
// Digilent PmodAD1 AXI4-Stream interface.
//
// ------------------------------------------------------------------

module axis_pmod_ad1_fsm (
      // ------------------------------------------------------------
      // AXI4-Stream Reset and Clock
      // ------------------------------------------------------------
      //
      input  logic rst_n,
      input  logic clk,

      // ------------------------------------------------------------
      // AXI4-Stream Master
      // ------------------------------------------------------------
      //
      input  logic axis_ready,

      // ------------------------------------------------------------
      // SPI Read-only Interface
      // ------------------------------------------------------------
      //
      output logic spi_sel_n,
      output logic spi_sck,

      // ------------------------------------------------------------
      // Datapath Control
      // ------------------------------------------------------------
      //
      output logic baud_en,
      output logic baud_load,
      input  logic baud_done,
      output logic shift_en,
      output logic shift_load,
      input  logic shift_done,
      output logic data_load

   );

   // ------------------------------------------------------------
   // FSM enumeration
   // ------------------------------------------------------------
   //
   enum {
      S_IDLE,   // SEL deasserted, SCK high, wait for ready
      S_START,  // SEL asserted, SCK high
      S_LOW,    // SCK is low
      S_HIGH,   // SCK is high
      S_END     // SEL deasserted, wait for (t8+tQUIET)
   } state = S_IDLE;

   // ------------------------------------------------------------
   // Internal signals
   // ------------------------------------------------------------
   //
   // Combinatorial signals
   logic c_spi_sel_n  = 1'b1;
   logic c_spi_sck    = 1'b1;
   logic c_baud_en    = 1'b0;
   logic c_baud_load  = 1'b0;
   logic c_shift_en   = 1'b0;
   logic c_shift_load = 1'b0;
   logic c_data_load  = 1'b0;

   // ------------------------------------------------------------
   // FSM state transitions
   // ------------------------------------------------------------
   //
   always_ff @(negedge rst_n or posedge clk)
   begin
      if (~rst_n) begin
         state <= S_IDLE;
      end
      else begin
         case (state)

            // Wait for AXI4-Stream ready
            S_IDLE:
               if (axis_ready) begin
                  state <= S_START;
               end

            // Assert SEL, leave SCK high
            S_START:
               begin
                  if (baud_done) begin
                     state <= S_LOW;
                  end
               end

            // SCK is low
            S_LOW:
               begin
                  if (baud_done) begin
                     state <= S_HIGH;
                  end
               end

            // SCK is high
            S_HIGH:
               begin
                  if (baud_done) begin
                     if (~shift_done) begin
                        state <= S_LOW;
                     end
                     else begin
                        state <= S_END;
                     end
                  end
               end

            // Deassert SPI controls
            S_END:
               begin
                  if (baud_done) begin
                     state <= S_IDLE;
                  end
               end
         endcase
      end
   end

   // ------------------------------------------------------------
   // FSM combinatorial output generation
   // ------------------------------------------------------------
   //
   always_comb
   begin
      // Defaults
      c_spi_sel_n  = 1'b1;
      c_spi_sck    = 1'b1;
      c_baud_en    = 1'b0;
      c_baud_load  = 1'b0;
      c_shift_en   = 1'b0;
      c_shift_load = 1'b0;
      c_data_load  = 1'b0;

      // Output generation
      case (state)
         // Wait for AXI4-Stream ready
         S_IDLE:
            if (axis_ready) begin
               // Assert SPI select
               c_spi_sel_n = 1'b0;

               // Load the baud-rate counter
               c_baud_load = 1'b1;

               // Load the shift bit counter
               c_shift_load = 1'b1;
            end

         // Assert SEL, leave SCK high
         S_START:
            begin
               // Assert SPI select/sync
               c_spi_sel_n = 1'b0;

               if (~baud_done) begin
                  // Enable the baud-rate counter
                  c_baud_en = 1'b1;
               end
               else begin
                  // Reload the baud-rate counter
                  c_baud_load = 1'b1;

                  // SPI clock low
                  c_spi_sck = 1'b0;
               end
            end

         // SCK is low
         S_LOW:
            begin
               // Assert SPI select/sync
               c_spi_sel_n = 1'b0;

               if (~baud_done) begin
                  // Enable the baud-rate counter
                  c_baud_en = 1'b1;

                  // SPI clock low
                  c_spi_sck = 1'b0;
               end
               else begin
                  // Reload the baud-rate counter
                  c_baud_load = 1'b1;
               end
            end

         // SCK is high
         S_HIGH:
            begin
               if (~baud_done) begin
                  // Assert SPI select/sync
                  c_spi_sel_n = 1'b0;

                  // Enable the baud-rate counter
                  c_baud_en = 1'b1;
               end
               else begin
                  // Reload the baud-rate counter
                  c_baud_load = 1'b1;

                  if (~shift_done) begin
                     // Assert SPI select/sync
                     c_spi_sel_n = 1'b0;

                     // SPI clock low
                     c_spi_sck = 1'b0;

                     // Enable the shift-register and bit counter
                     c_shift_en = 1'b1;
                  end
                  else begin
                     // Load the AXI4-Stream data register
                     c_data_load = 1'b1;
                  end
               end
            end

         // Deassert SPI controls
         S_END:
            begin
               if (~baud_done) begin
                  // Enable the baud-rate counter
                  c_baud_en = 1'b1;
               end
            end

      endcase
   end

   // ------------------------------------------------------------
   // Outputs
   // ------------------------------------------------------------
   //
   // Registered outputs
   always_ff @(negedge rst_n or posedge clk)
   begin
      if (~rst_n) begin
         spi_sel_n  = 1'b1;
         spi_sck    = 1'b1;
      end
      else begin
         spi_sel_n  <= c_spi_sel_n;
         spi_sck    <= c_spi_sck;
      end
   end

   // Combinatorial outputs
   assign baud_en    = c_baud_en;
   assign baud_load  = c_baud_load;
   assign shift_en   = c_shift_en;
   assign shift_load = c_shift_load;
   assign data_load  = c_data_load;

endmodule