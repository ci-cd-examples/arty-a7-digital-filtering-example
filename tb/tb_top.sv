// Top level testbench for Arty A7 Pmod AD1/DA4 Filter example design

`timescale 1ns / 1ps

`include "../src/hdl/interfaces.sv"

module tb_top;
    // ---------------------
    // FIR Filter parameters
    // ---------------------
    localparam ADC_RES_BITS = 16;                                           // Bit width resolution of targetted ADC
    localparam ADC_RES_BITS_REAL = 12;                                      // Actual bit width resolution of ADC
    localparam COEFF_BIT_WIDTH = 18;                                        // Bit width of FIR filter coefficients
    // Bit width for FIR filter input data (incremented by set of 8 bits for AXI)
    localparam INPUT_BIT_WIDTH = ADC_RES_BITS % 8 > 0 ? int'($ceil(ADC_RES_BITS/8.0)*8) : ADC_RES_BITS;
    // Bit width for FIR filter output data (incremented by set of 8 bits for AXI)
    localparam OUTPUT_BIT_WIDTH = (ADC_RES_BITS + COEFF_BIT_WIDTH) % 8 > 0 ? int'($ceil((ADC_RES_BITS + COEFF_BIT_WIDTH)/8.0)*8) : ADC_RES_BITS + COEFF_BIT_WIDTH;
    localparam MSB_FILTER = INPUT_BIT_WIDTH + (ADC_RES_BITS-1);             // MSB of filtered data to start pulling from

    // ADC saling factors
    localparam ADC_SAMPLE_BIT_RANGE = 12;                                   // Range of generated ADC data
    localparam ADC_FACTOR = 3.3 / real'(2**ADC_SAMPLE_BIT_RANGE);

    // Clock and Sine wave generation
    localparam ACLK_HZ = 100_000_000;                                       // AXI Clock Frequency (100 MHz)
    localparam ACLK_TIME = 0.5 * 1_000_000_000 / ACLK_HZ;
    localparam SAMPLE_CLK_DIV = 10_000;
    localparam SIN_AMPL = 1.1*((2**ADC_SAMPLE_BIT_RANGE)/2-1);             // Run at 99% maximum dynamic range
    localparam SIN_OFFSET = SIN_AMPL;                                       // Offset aplitude to start at 0 for unsigned Sine waves
    localparam FREQ_START = 10;
    localparam FREQ_END = 100;
    localparam FREQ_STEP = 20;

    // Testbench clock and reset
    logic tb_aclk;
    logic tb_rst_n;

    // Testbench signals
    logic data_valid = 0;
    logic [INPUT_BIT_WIDTH-1:0] adc_sample_out;     // Sample sine wave data to mimic ADC reads

    // Simulated onboard components
    logic  [3:0] sw;
    logic  [3:0] btn;
    logic  [7:0] LED;

    // Simulated Pmod ports
    logic [10:7] ja;
    logic [10:7] jd;

    top DUT (
        // Onboard oscillator
        .CLK100MHZ  (tb_aclk),

        // ChipKit Single Ended Analog Inputs
        .ck_an_p    (),
        .ck_an_n    (),

        // Dedicated ADC channel
        .vp_in      (),
        .vn_in      (),

        // Onboard switches and LEDs
        .sw         (sw),
        .btn        (btn),
        .LED        (LED),

        // Pmod port signals
        .ja_7       (ja[7]),
        .ja_8       (ja[8]),
        .ja_9       (ja[9]),
        .ja_10      (ja[10]),
        .jd_7       (jd[7]),
        .jd_8       (jd[8]),
        .jd_9       (jd[9]),
        .jd_10      (jd[10])
    );

    // --------------------------------------------------------
    //                   BIT TRUE MODEL
    // --------------------------------------------------------
    // Instantiate the FIR Compiler Core
    logic [OUTPUT_BIT_WIDTH-1:0] filtered_data_bit_true;
    logic [ADC_RES_BITS-1:0] filter_trunc_msb_bit_true;
    assign filter_trunc_msb_bit_true = {filtered_data_bit_true[$size(filtered_data_bit_true)-1], filtered_data_bit_true[MSB_FILTER -: (ADC_RES_BITS-1)]};
    fir_compiler_0 adc_filter_bit_true (
        .aclk               (tb_aclk),                  // AXI Clock
        .aresetn            (tb_rst_n),                 // Reset (Active low)
        .s_axis_data_tvalid (data_valid),               // Input: Valid data (to be raised when sample is valid)
        .s_axis_data_tready (),                         // Output: Filter is ready for another input
        .s_axis_data_tdata  (adc_sample_out),           // Input: Data to be filtered
        .m_axis_data_tvalid (),                         // Output: Data valid signal
        .m_axis_data_tdata  (filtered_data_bit_true)    // Output: Filtered data (output)
    );

    // Generate testbench AXI clock
    always #(ACLK_TIME) tb_aclk = !tb_aclk; // 100 MHz clock

    // Hardware assignments
    assign btn[0] = !tb_rst_n;
    assign DUT.ad1_if.axis_data[31:12] = 20'b0;
    assign DUT.ad1_if.axis_data[11:0] = adc_sample_out[11:0];
    
    // Generates Sine wave chirp for test stimulus
    // sin_ampl: Amplitude of Sine wave
    // sin_offset: Offset of Sine wave
    // sin_freq: Frequency of Sine was (Hz)
    // run_time_us: Total run time of Chirp (us)
    // sample_clk_div: Clock divider of top level testbench clock
    //                 Example: sample_clk_div=1000 --> data_valid goes high
    //                 every 1000 clocks of the testbench clock
    task chirp_gen(int sin_ampl, int sin_offset, int sin_freq, real run_time_us, int sample_clk_div);
        localparam PI = 3.14159265359;
        real start_time_us;        // Starting time of task
        real time_us, time_s;

        // Used to generate data_valid signal
        int sampling_counter;
        int adc_sample_value;     // Sample sine wave data to mimic ADC reads

        $display("Running %0d Hz Sine wave generation for %0.3f ms.", sin_freq, run_time_us/1000);

        @ (posedge tb_aclk);
        start_time_us = $time/1000;        // Get current starting time of task
        sampling_counter = 0;

        do begin
            // Get current time
            time_us = $time/1000;
            time_s = time_us/1000000;

            // Generate sample based on desired frequency
            @ (posedge tb_aclk);
            adc_sample_value = sin_offset + (sin_ampl * $sin(2*PI*sin_freq*time_s));
            
            // Saturation handling
            if (adc_sample_value > (2**ADC_SAMPLE_BIT_RANGE)-1)
                adc_sample_out = (2**ADC_SAMPLE_BIT_RANGE)-1;
            else if (adc_sample_value < 0)
                adc_sample_out = 0;
            else
                adc_sample_out = adc_sample_value[INPUT_BIT_WIDTH-1:0];
            
            // Generate data_valid strobe
            if (sampling_counter < sample_clk_div) begin
                sampling_counter++;
                data_valid = 1'b0;
            end
            else begin
                sampling_counter = 0;
                data_valid = 1'b1;
            end
        end while (time_us - start_time_us < run_time_us);
    endtask

    initial begin
        // Initialize clock
        tb_aclk = 0;
        sw = 0;
        btn[3:1] = 0;

        // De-assert reset
        tb_rst_n = 0;
        #10_000;
        tb_rst_n = 1;
        
        #5_000_000;

        //for (real freq = FREQ_START; freq <= FREQ_END; freq = freq + FREQ_STEP) begin
        //    chirp_gen(SIN_AMPL, SIN_OFFSET, freq, 1/freq * 1_000_000 * 5, SAMPLE_CLK_DIV);
        //    chirp_gen(0, 0, 0, 1_000, SAMPLE_CLK_DIV);
        //end

        chirp_gen(SIN_AMPL, SIN_OFFSET, 5, 1_000_000 * 5, SAMPLE_CLK_DIV);

        #5_000_000;
        
        $finish;
    end

endmodule